package com.date;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UserLikedProfiles extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    GridView container;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = new DatabaseHelper(this);
        setContentView(R.layout.user_liked_profiles_container);
        container = findViewById(R.id.container_user_liked_profiles);

        ArrayList<Profile> list = databaseHelper.getLikedList("liked");
        Liked_Profiles_Adapter adapter = new Liked_Profiles_Adapter(this,list);

        container.setAdapter(adapter);
    }
}
