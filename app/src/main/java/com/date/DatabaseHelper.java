package com.date;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    static String name = "Date";
    static int version = 1;

    String CreateTableLiked = "CREATE TABLE if not exists `liked` (\n" +
            "\t`id`\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\t`name`\tTEXT,\n" +
            "\t`age`\tTEXT,\n" +
            "\t`location`\tTEXT,\n" +
            "\t`url`\tTEXT \n" +
            ")";

    String CreateTableRejected= "CREATE TABLE if not exists `rejected` (\n" +
            "\t`id`\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\t`name`\tTEXT,\n" +
            "\t`age`\tTEXT,\n" +
            "\t`location`\tTEXT,\n" +
            "\t`url`\tTEXT \n" +
            ")";

    public DatabaseHelper(Context context) {
        super(context,name,null,version);
        getWritableDatabase().execSQL(CreateTableLiked);
        getWritableDatabase().execSQL(CreateTableRejected);
    }

    public void clearAllDatabase(){
        getWritableDatabase().execSQL("delete from liked");
        getWritableDatabase().execSQL("delete from rejected ");
    }

    public void insert(String name,Profile profile){
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",profile.getName());
        contentValues.put("age",profile.getAge());
        contentValues.put("location",profile.getLocation());
        contentValues.put("url",profile.getImageUrl());

        Log.i("insertUsers", "insert: "+getWritableDatabase()
                .insert(name,"",contentValues));
    }

    public ArrayList<Profile> getLikedList(String dbName) {
        String sql = "select * from "+dbName;
        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        Log.i("cursorsize", "Cursor size:" + cursor.getCount());
        ArrayList<Profile> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            Profile profile = new Profile();
            profile.setName(cursor.getString(cursor.getColumnIndex("name")));
            int age = Integer.parseInt(cursor.getString(cursor.getColumnIndex("age")));
            profile.setAge(age);
            profile.setLocation(cursor.getString(cursor.getColumnIndex("location")));
            profile.setImageUrl(cursor.getString(cursor.getColumnIndex("url")));
            list.add(profile);
        }
        cursor.close();
        return list;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table liked ");
        db.execSQL("Drop table rejected ");
    }
}
