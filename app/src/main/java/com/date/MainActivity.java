package com.date;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.Placeholder;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.date.Animation.MyBounceInterpolator;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.skyfishjy.library.RippleBackground;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    CircularImageView profile_pic;
    CircularImageView map;
    public Button invite;
    LinearLayout invite_container;

    View view;
    SwipePlaceHolderView mSwipeView;
    Context mContext;
    Activity activity;
    FrameLayout frameLayout;

    public void findNecessaryComponents() {
        profile_pic = findViewById(R.id.home_image_user);
        map = findViewById(R.id.home_image_map);
        invite = findViewById(R.id.home_invite_friends);
        invite_container = findViewById(R.id.home_invite_container);

        mSwipeView = (SwipePlaceHolderView) findViewById(R.id.home_swipeView);
        mContext = getApplicationContext();
        activity = getParent();
        frameLayout = findViewById(R.id.home_frame_layout);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        view = getLayoutInflater().from(this).inflate(R.layout.activity_main,null);
        setContentView(view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        findNecessaryComponents();

        final Animation myAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 15);
        myAnim.setInterpolator(interpolator);
        profile_pic.setAnimation(myAnim);

        RippleBackground rippleDrawable = (RippleBackground) findViewById(R.id.ripple_container);
        rippleDrawable.startRippleAnimation();

        profile_pic.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                profile_pic.startAnimation(myAnim);
            }
        });

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSwipeView.getChildCount() > 1){
                    invite_container.setVisibility(View.GONE);
                    frameLayout.setVisibility(View.VISIBLE);
                }
                else{
                    Toast.makeText(mContext, "no new users", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.tinder_swipe_in_msg_view)
                        .setSwipeOutMsgLayoutId(R.layout.tinder_swipe_out_msg_view));

        for (Profile profile : Utils.loadProfiles(mContext)) {
            final Profile useProfile = profile;
            TinderCard card = new TinderCard(mContext, profile, mSwipeView,activity);
            card.containers(frameLayout,invite_container);
            mSwipeView.addView(card);
        }

        view.findViewById(R.id.home_rejectBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwipeView.doSwipe(false);
            }
        });

        view.findViewById(R.id.home_acceptBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwipeView.doSwipe(true);
            }
        });
    }

    public void scaleanimation() {
        ScaleAnimation animation = new ScaleAnimation(1.0f, 1.5f, 1.0f, 1.5f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setDuration(500);
        profile_pic.startAnimation(animation);
    }

    public void animation1() {
        ObjectAnimator scaleDownX;
        ObjectAnimator scaleDownY;
        AnimatorSet scaleDown;

        scaleDownX = ObjectAnimator.ofFloat(profile_pic, "scaleX", 1.5f);
        scaleDownY = ObjectAnimator.ofFloat(profile_pic, "scaleY", 1.5f);
        scaleDownX.setDuration(500);
        scaleDownY.setDuration(500);

        scaleDown = new AnimatorSet();
        scaleDown.play(scaleDownX).with(scaleDownY);
        scaleDown.start();
    }

    Boolean exitApplication = false;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (exitApplication) {
                super.onBackPressed();
                finish();
            } else {
                exitApplication = true;
                Toast.makeText(this, "Press back again to exit", Toast.LENGTH_LONG).show();
            }
            findViewById(R.id.drawer_layout).postDelayed(new Runnable() {
                @Override
                public void run() {
                    exitApplication = false;
                }
            }, 4000);
//            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

        }
        else if(id == R.id.action_clear_all){
            new DatabaseHelper(this).clearAllDatabase();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_liked) {
            startActivity(new Intent(MainActivity.this,
                    UserLikedProfiles.class));

        } else if (id == R.id.nav_rejected) {
            startActivity(new Intent(MainActivity.this,
                    UserRejectedProfiles.class));

        } else if (id == R.id.nav_chat) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
